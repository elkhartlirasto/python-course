## Compulsory Software

Download and install **Git**:

- For Windows:
  - Download from http://git-scm.com/download/win
  - Install
  - Launch Git Bash

- For Mac:
  - Download from http://git-scm.com/download/mac
  - Install

- For Linux: `apt-get install git`

After this, run the following command in the terminal:

$ `git config --list`

If the user.name and user.email sections are blank or absent altogether, set your global credentials by running the following commands in the terminal (use your GitHub credentials for this):

$ `git config --global user.name "John Doe"`  
$ `git config --global user.email johndoe@example.com`
